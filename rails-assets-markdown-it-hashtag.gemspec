# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-markdown-it-hashtag/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-markdown-it-hashtag"
  spec.version       = RailsAssetsMarkdownItHashtag::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "hashtag for markdown-it markdown parser"
  spec.summary       = "hashtag for markdown-it markdown parser"
  spec.homepage      = "https://github.com/svbergerem/markdown-it-hashtag"
  spec.license       = "MIT"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
